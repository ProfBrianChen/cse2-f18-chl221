
//Damon Luk
//10_8_18
//CSE 002 - HW 05 

import java.util.Scanner; //imports the scanner method 
public class Hw05 {
  public static void main (String[]args) {
    Scanner myScanner = new Scanner ( System. in ); //creates an instance of the scanner
    int counter = 0; //declares a variable counter that will be used later
    int x = 0; //counter that will run till the five cards are tested for each hand
    int number = 0; // declares a variable that will randomly be assigned a number 1 -52 later on 
    int fourOfAKind = 0; //declares a counter for four of a kind hands
    int threeOfAKind = 0; //declares a counter for three of a kind hands
    int twoPair = 0; //declares a counter for two pair hands
    int onePair = 0; //declares a counter for one pair hands
    int hands = 0; //declares a variable and initialize it to zero
    int lastOnePair = 0; //declares a variable and initialize it to zero
   
    
    
    System.out.println ("How many hands do you want to generate? "); // asks the user how many times they want to generate hands
    if (myScanner.hasNextInt() == true ) { //tests to see if the user input an integer
       hands = myScanner.nextInt();
    }
    else {
      System.out.println ("Error. Please enter an integer."); //if not it tells them to enter again
      myScanner.next();
    }
   
    while ( counter < hands) { //while the counter is less than the number that the user wants, this loop will run
    		int card1 = 0; //counter to count how many times ace appears
        int card2 = 0; //counter to count how many times 2 appears
        int card3 = 0; //counter to count how many times 3 appears
        int card4 = 0; //counter to count how many times 4 appears
        int card5 = 0; //counter to count how many times 5 appears
        int card6 = 0; //counter to count how many times 6 appears
        int card7 = 0; //counter to count how many times 7 appears
        int card8 = 0; //counter to count how many times 8 appears
        int card9 = 0; //counter to count how many times 9 appears
        int card10 = 0; //counter to count how many times 10 appears 
        int card11 = 0; //counter to count how many times 11 appears
        int card12 = 0; //counter to count how many times 12 appears
        int card13 = 0; //counter to count how many times 13 appears
        x = 0;
    	
    		while ( x < 5 ) { //generates random numbers for one hand 
    			number = (int)(Math.random() * 52 + 1); //picks a random number from 1 -  52 for card one
    			int remainder = number % 13; //divides the number by 13 and saves the remainder 
          //System.out.println ( remainder ); This was used to debug my code
    			switch ( remainder ) { //switch statement to evaluate the remainder
    			case 1: //if the remainder is 1, the card1 counter gets a plus one
    				card1++;
    			break;
    			case 2: //if the remainder is 2, the card2 counter gets a plus one
    				card2++;
    			break;
    			case 3: //if the remainder is 3, the card3 counter gets a plus one
    				card3++;
    			break;
    			case 4: //if the remainder is 4, the card4 counter gets a plus one
    				card4++;
          break;
    			case 5: //if the remainder is 5, the card5 counter gets a plus one
    				card5++;
    			break;
    			case 6: //if the remainder is 6, the card6 counter gets a plus one
    				card6++;
    			break;
    			case 7: //if the remainder is 7, the card7 counter gets a plus one
    				card7++;
    			break;
    			case 8: //if the remainder is 8, the card8 counter gets a plus one
    				card8++;
    			break;
    			case 9: //if the remainder is 9, the card9 counter gets a plus one
    				card9++;
    			break;
    			case 10: //if the remainder is 10, the card10 counter gets a plus one
    				card10++;
    			break;
    			case 11: //if the remainder is 11, the card11 counter gets a plus one
    				card11++;
    			break;
    			case 12: //if the remainder is 12, the card12 counter gets a plus one
    				card12++;
    			break;
    			case 0: //if the remainder is 13, the card13 counter gets a plus one
    				card13++;
    			break;
    			}
    			
    		x++; //the x  counter gets a plus one 
          
        }
    		
      //if any of the counter equals 4 then it is a four of a kind and its counter gets a plus one
    		if ( card1 == 4 || card2 == 4 || card3 == 4 || card4 == 4 || card5 ==4 || card6 == 4 || card7 == 4 || card8 == 4) { 
    			fourOfAKind++;
    		} 
      //same as above
    		else if ( card9 == 4 || card10 == 4 || card11 == 4 || card12 == 4 || card13 == 4 ) {
    			fourOfAKind++;
    		}
      //if any of the counter equals 3 then it is a three of a kind and its counter gets a plus one
    		else if ( card1 == 3 || card2 == 3 || card3 == 3 || card4 == 3 || card5 == 3 || card6 == 3 || card7 == 3 || card8 == 3) { 
    			threeOfAKind++;
    		}
      //same as above
    		else if ( card9 == 3 || card10 == 3 || card11 == 3 || card12 == 3 || card13 == 3 ) {
    			threeOfAKind++;
    		}
      //if any of the card counter equals 2 then it is a pair 
    		if ( card1 == 2 ) {
    			onePair++;
    		}
    		if ( card2 == 2 ) {
    			onePair++;
    		}
    		if ( card3 == 2 ) {
    			onePair++;
    		}
    		if ( card4 == 2 ) {
    			onePair++;
    		}
    		if ( card5 == 2 ) {
    			onePair++;
    		}
    		if ( card6 == 2 ) {
    			onePair++;
    		}
    		if ( card7 == 2 ) {
    			onePair++;
    		}
    		if ( card8 == 2 ) {
    			onePair++;
    		}
    		if ( card9 == 2 ) {
    			onePair++;
    		}
    		if ( card10 == 2 ) {
    			onePair++;
    		}
    		if ( card11 == 2 ) {
    			onePair++;
    		}
    		if ( card12 == 2 ) {
    			onePair++;
    		}
    		if ( card13 == 2 ) {
    			onePair++;
    		}
    		if ( onePair == lastOnePair + 2 ) { //if there are two one pairs then it is a two pair
    			twoPair++; //adds one to the two pair counter
    			onePair -= 2; //subract 2 from the one pair 
    		}
    		
    		lastOnePair = onePair; // used to see if there is a two pair
    		counter++; //adds one to a counter 
    		
    }
    		
   // System.out.println (+fourOfAKind); //these were used to bug my code
   // System.out.println (+threeOfAKind);
   // System.out.println (+twoPair);
   // System.out.println (+onePair);
    
    double probability4  = ((double) fourOfAKind) / hands; //these are used to calculate my probability
    double probability3 = ((double) threeOfAKind) / hands;
    double probability2 = ((double) twoPair) / hands;
    double probability1 = ((double) onePair) / hands;
    
    //prints my output
    System.out.println ("The number of loops: " + hands );
    System.out.printf ("The probability of Four-Of-a-kind: %4.3f\n", probability4 );
    System.out.printf ("The probability of Three-Of-a-kind: %4.3f\n", probability3 );
    System.out.printf ("The probability of Two-pair: %4.3f\n ", probability2 );
    System.out.printf ("The probability of a One-pair: %4.3f\n ", probability1);
    
  }
}

    
    
    
    
    
    
    

