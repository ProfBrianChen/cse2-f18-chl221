//Damon Luk
//9_21_18
//CSE 002- HW 04

import java.util.Scanner; //imports the scanner method
public class CrapsIf{
  public static void main (String[]args) {
    Scanner myScanner = new Scanner ( System.in ); //declares an instance of the scanner
    //asks the user which one they prefer 
    System.out.println ("Would you like to randomly cast dice or evaluate two dice? (Enter 1 for cast and 2 to evaluate)"); 
    int answer = myScanner.nextInt(); //stores the user's answer as a string variable
    
    if ( answer == 1 ) {
      int dieOne = (int)(Math.random() * 6 + 1); //generates the first random dice 
      int dieTwo = (int)(Math.random() * 6 + 1); //generates the second random dice
      
      //System.out.println (""+dieOne+" "+dieTwo+""); //checks to see if the random generator works
      
      int remainder1 = dieOne % 6; //divides the first die by 6 and stores its remainder
      int remainder2 = dieTwo % 6; //divides the second die by 6 and stores its remainder
      
      //checks to see if the remainders equal each other
      //if they do then checks to see what dieOne is to determine outcome
      //prints outcome
      if ( remainder1 == remainder2 ) { 
        if ( dieOne == 1) {  
          System.out.println ("You rolled a Snake Eyes"); 
        }
        if ( dieOne == 2) { 
          System.out.println ("You rolled a Hard Four"); 
        }
        if ( dieOne == 3) { 
          System.out.println ("You rolled a Hard Six"); 
        }
        if ( dieOne == 4) { 
          System.out.println ("You rolled a Hard Eight");
        }
        if ( dieOne == 5) {
          System.out.println ("You rolled a Hard Ten");
        }
        if ( dieOne == 6 ){
          System.out.println ("Your rolled a Boxcar");
        }
      }
     else if ( dieOne + dieTwo == 3 ) { //if the two dice add to 3 then it is an ace deuce
        System.out.println ("You rolled an Ace Deuce"); //prints outcome
      }
     else if ( dieOne + dieTwo == 4) { //if the two dice add to 4 then it is an easy four
        System.out.println ("You rolled an Easy Four"); //prints outcome
      }
     else if ( dieOne + dieTwo == 5) { //if the two dice add to 5 then it is a fever five
        System.out.println ("You rolled a Fever Five"); //prints outcome
      }
     else if ( dieOne + dieTwo == 6) { //if the two dice add to 6 then it is an easy six
        System.out.println ("You rolled an Easy Six"); //prints outcome
      }
     else if ( dieOne + dieTwo == 7) { //if the two dice add to 7 then it is a seven out
        System.out.println ("You rolled a Seven Out"); //prints outcome
      }
     else if ( dieOne + dieTwo == 8) { //if the two dice add to 8 then it is an easy eight
        System.out.println ("You rolled an Easy Eight"); //prints outcome
      }
     else if ( dieOne + dieTwo == 9) { //if the two dice add to 9 then it is a nine
        System.out.println ("You rolled a Nine"); //prints outcome
      }
     else if ( dieOne + dieTwo == 10) { //if the two dice add to 10 then it is an easy ten
        System.out.println ("You rolled an Easy Ten"); //prints outcome
      }
     else if ( dieOne + dieTwo ==11) { //if the two dice add to 11 then it is a yo-leven
        System.out.println ("You rolled a Yo-leven"); //prints outcome
      }
      }
    else if ( answer == 2) {
      System.out.println ("Please enter the first die: "); //asks the user to submit the first number
      int numberOne = myScanner.nextInt(); //accepts the response and stores it as an integer
      System.out.println ("Please enter the second die: "); //asks the user to submit the second number 
      int numberTwo = myScanner.nextInt(); //accepts the response and stores it as an integer
      
      if ( numberOne < 1 || numberOne > 6 || numberTwo < 1 || numberTwo > 6) { //checks to see if the user's response is valid
        System.out.println ("Please enter a number between 1 and 6");       
      }
      
      int remainder3 = numberOne % 6; //divides number one by 6 and stores its remainder
      int remainder4 = numberTwo % 6; //divides number two by 6 and stores its remainder
      
      //checks to see if the remainders equal each other
      //if they do then checks to see what numberOne is to determine outcome
      //prints outcome
      if ( remainder3 == remainder4 ) { 
        if ( numberOne == 1) {  
          System.out.println ("You rolled a Snake Eyes"); 
        }
        if ( numberOne == 2) { 
          System.out.println ("You rolled a Hard Four"); 
        }
        if ( numberOne == 3) { 
          System.out.println ("You rolled a Hard Six"); 
        }
        if ( numberOne == 4) { 
          System.out.println ("You rolled a Hard Eight");
        }
        if ( numberOne == 5) {
          System.out.println ("You rolled a Hard Ten");
        }
        if ( numberOne == 6 ){
          System.out.println ("Your rolled a Boxcar");
        }
      }
     else if ( numberOne + numberTwo == 3 ) { //if the two numbers add to 3 then it is an ace deuce
        System.out.println ("You rolled an Ace Deuce"); //prints outcome
      }
     else if ( numberOne + numberTwo == 4) { //if the two numbers add to 4 then it is an easy four
        System.out.println ("You rolled an Easy Four"); //prints outcome
      }
     else if ( numberOne + numberTwo == 5) { //if the two numbers add to 5 then it is a fever five
        System.out.println ("You rolled a Fever Five"); //prints outcome
      }
     else if ( numberOne + numberTwo == 6) { //if the two numbers add to 6 then it is an easy six
        System.out.println ("You rolled an Easy Six"); //prints outcome
      }
     else if ( numberOne + numberTwo == 7) { //if the two numbers add to 7 then it is a seven out
        System.out.println ("You rolled a Seven Out"); //prints outcome
      }
     else if ( numberOne + numberTwo == 8) { //if the two numbers add to 8 then it is an easy eight
        System.out.println ("You rolled an Easy Eight"); //prints outcome
      }
     else if ( numberOne + numberTwo == 9) { //if the two numbers add to 9 then it is a nine
        System.out.println ("You rolled a Nine"); //prints outcome
      }
     else if ( numberOne + numberTwo == 10) { //if the two numbers add to 10 then it is an easy ten
        System.out.println ("You rolled an Easy Ten"); //prints outcome
      }
     else if ( numberOne + numberTwo ==11) { //if the two numbers add to 11 then it is a yo-leven
        System.out.println ("You rolled a Yo-leven"); //prints outcome
    }
    
     
    }
}
}
  
    
      
    
 