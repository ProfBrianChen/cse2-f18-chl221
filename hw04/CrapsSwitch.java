//Damon Luk
//9_23_18
//CSE 002 - HW04

import java.util.Scanner; //imports the scanner method
public class CrapsSwitch{
  public static void main (String[]args) {
    Scanner myScanner = new Scanner ( System.in ); //declares an instance of the scanner
    //asks the user which one they prefer 
    System.out.println ("Would you like to randomly cast dice or evaluate two dice? (Enter 1 for cast and 2 to evaluate)"); 
    int answer = myScanner.nextInt(); //stores the user's answer as a string variable
    
    switch ( answer ) { //switch statement evaluating the answer the user inputs
      case 1: //when the user wants randomly cast dice
      int dieOne = (int)(Math.random() * 6 + 1); //generates the first random dice 
      int dieTwo = (int)(Math.random() * 6 + 1); //generates the second random dice
      
      //System.out.println (""+dieOne+" "+dieTwo+""); //prints out the two dice to check. Is a comment once code is finished.
      
      int sum1 = dieOne + dieTwo; //adds the two dice together 
      
        //all breaks leaves the case that was being evaluated 
        switch ( sum1 ) { //switch statement evaluating the int value sum1
        case 2: //when sum1 = 2 
          System.out.println ( "You rolled a Snake Eyes"); //prints Snake Eyes
        break; 
        case 3: //when sum1 = 3 
          System.out.println ( "You rolled an Ace Deuce"); //prints Ace Deuce
        break;  
        case 4: //when sum1 = 4
          switch ( dieOne ) { //switch statement evaluating dieOne when the sum is 4
            case 2: //when dieOne is two
              System.out.println ( "You rolled a Hard Four"); //prints Hard Four
            break; 
            default: //the default case when dieOne is not 2
              System.out.println ( "You rolled an Easy Four"); //prints Easy Four
            break; 
            }
        break;  
        case 5://when sum1 = 5
          System.out.println ( "You rolled a Fever Five"); //prints Fever Five
        break; 
        case 6: //when sum1 = 6
          switch ( dieOne ) { //switch statement evaluating dieOne when the sum is 6
           case 3: //when dieOne is 3 
              System.out.println ( "You rolled a Hard Six"); //prints Hard Six
            break;
            default: //the default case when dieOne is not 3
              System.out.println ( "You rolled an Easy Six"); //prints Easy Six
            break;
          }
        break;
        case 7: //when sum1 = 7  
          System.out.println ( "You rolled a Seven Out"); //prints Seven Out
        break;
        case 8: //when sum1 = 8
          switch ( dieOne ) { ////switch statement evaluating dieOne when the sum is 8
            case 4: //when dieOne is 4 
              System.out.println ( "You rolled a Hard Eight"); //prints Hard Eight
            break;
            default: //the default case when dieOne is not 4
              System.out.println ( "You rolled an Easy Eight"); //prints Easy Eight
            break;
          }
        break;
        case 9: //when sum1 = 9
          System.out.println ( "You rolled a Nine"); //prints Nine
        break;
        case 10: //when sum1 = 10
          switch ( dieOne ) { ////switch statement evaluating dieOne when the sum is 10
            case 5: //when dieOne is 5
              System.out.println ( "You rolled a Hard Ten"); //prints Hard Ten
            break;
            default: //the default case when dieOne is not 5
              System.out.println ( "You rolled an Easy Ten"); //prints Easy Ten
            break;
          }
        break;
        case 11: //when sum1 = 11
          System.out.println ( "You rolled a Yo-leven"); //prints Yo leven
        break;
        case 12: //when sum1 = 12 
          System.out.println ( "You rolled Boxcars"); //prints Boxcars
        break;
      }
      break;
      case 2: //when the user wants to input numbers
      System.out.println ("Please enter the first die: "); //asks the user to submit the first number
      int numberOne = myScanner.nextInt(); //accepts the response and stores it as an integer
      System.out.println ("Please enter the second die: "); //asks the user to submit the second number 
      int numberTwo = myScanner.nextInt(); //accepts the response and stores it as an integer
      
      int sum2 = numberOne + numberTwo; //adds up the two numbers 
        
      //same logic as above
      //tests each sum and uses switch statements to determine outcome 
      switch ( sum2 ) {
        default: //tests to see if the numbers are in range
          System.out.println ( "Please enter a value between 1 and 6"); //tells the user to put in numbers within range
        break;
        case 2:
          System.out.println ( "You rolled a Snake Eyes");
        break;
        case 3:
          System.out.println ( "You rolled an Ace Deuce");
        break;
        case 4:
          switch ( numberOne ) {
            case 2:
              System.out.println ( "You rolled a Hard Four");
            break;
            default:
              System.out.println ( "You rolled an Easy Four");
            break;
            }
        break;
        case 5:
          System.out.println ( "You rolled a Fever Five");
        break;
        case 6:
          switch ( numberOne ) {
           case 3:
              System.out.println ( "You rolled a Hard Six");
            break;
            default:
              System.out.println ( "You rolled an Easy Six");
            break;
          }
        break;
        case 7:
          System.out.println ( "You rolled a Seven Out");
        break;
        case 8:
          switch ( numberOne ) {
            case 4:
              System.out.println ( "You rolled a Hard Eight");
            break;
            default:
              System.out.println ( "You rolled an Easy Eight");
            break;
          }
        break;
        case 9:
          System.out.println ( "You rolled a Nine");
        break;
        case 10:
          switch ( numberOne ) {
            case 5:
              System.out.println ( "You rolled a Hard Ten");
            break;
            default: 
              System.out.println ( "You rolled an Easy Ten");
            break;
          }
        break;
        case 11: 
          System.out.println ( "You rolled a Yo-leven");
        break;
        case 12:
          System.out.println ( "You rolled Boxcars");
        break;
      }
    }
  }
}
        
          
              
              
     

      
      
      
      
      
      


      
      