//Damon Luk
//CSE 2 - HW 08 
//11_13_18


import java.util.Scanner; //imports the scanner method
import java.util.Random; //import the random generator method

public class Shuffling{ 
public static void main(String[] args) { //main method
Scanner scan = new Scanner(System.in); //creates an instance of the scanner
 //suits club, heart, spade or diamond 
 String[] suitNames={"C","H","S","D"};   //makes a string with the suit names 
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; //makes a string with the number of the cards 
String[] cards = new String[52]; //makes a string and allocates 52 spaces in it 
String[] hand = new String[5]; //makes a string and allocates 5 spaces in it
int numCards = 5; //declares variable numCards to be 5
int again = 1; //declares variable again to be 1 
int index = 51; //declares variable index to be 51
for (int i=0; i<52; i++) { //for loop that goes through every single space in the string cards
  cards[i]=rankNames[i%13]+suitNames[i/13]; //initializes all the spaces in string cards
  //System.out.print(cards[i]+" "); 
} 

printArray(cards); //uses the printArray method to print the deck
String[] shuffledDeck = shuffle(cards); //uses the shuffle method to shuffle the deck
System.out.println (); //prints a new line
System.out.println ("Shuffled"); //prints out the word shuffled
printArray(shuffledDeck); //prints the shuffled deck
System.out.println(); //prints a new line
System.out.println ("Hand"); //prints out the word Hand
while(again == 1 && index >= 5){ 
   hand = getHand(cards,index,numCards); //gets a hand
   printArray(hand); //prints the hand
   index -= numCards; //subracts the index
   System.out.println("Enter a 1 if you want another hand drawn"); //asks the user if they want to get another hand
   again = scan.nextInt(); //checks the response
}  
  } 
  
public static void printArray (String[]cards) { //the method print array that is used to print the deck and the hand eventually
  int size = cards.length; //sets the variable size to the length of the string
  for (int i = 0; i < size; i++) { //for loop that goes from 0 to the size of the deck
  System.out.print(cards[i]+" "); //prints the deck
  }
  
}
public static String[] shuffle (String[]cards) { //method that shuffles the deck
  Random randomGenerator = new Random (); //declares an instance of the random generator
  for (int x = 0; x < 69; x++) { //for loop that goes from 0 to 69 
    int j = randomGenerator.nextInt (52); //generates a random number from 0 to 51
    String old = cards [0]; //stores the first index in a string
    cards [0] = cards [j]; //replaces the first one with the card at the randomized index
    cards [j] = old; //replaces the randomized index with old which is the first index 
  }
  return cards; //returns cards to the main method to be printed out
}
public static String[] getHand (String[]cards, int index, int numCards) { //method used to get 5 cards for the hand
  String [] hand= new String [numCards]; //creates a new string and set its size to the variable numCards
  for (int n = 0; n < numCards; n++) { //for loop from 0 to numCards
    hand[n] = cards[index - n]; //sets the hand starting from the end of the list 
  }
  return hand; //returns the hand
  
}
}



