//Damon Luk
//9_13_18
//CSE 002 - Check

import java.util.Scanner; //this imports the new method 

public class Check {
  
  public static void main (String[]args) {
    Scanner myScanner = new Scanner ( System.in ); //this declares an instance of the Scanner
    
    System.out.print ("Enter the original cost of the check in the form xx.xx: "); //asks the user to input the cost of the check
    double checkCost = myScanner.nextDouble(); //accepts the user's response and stores it as a double
    
    //asks the user to input how much tip they want to give
    System.out.print ("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); 
    double tipPercent = myScanner.nextDouble(); //accepts the users's response and stores it as a double
    tipPercent /= 100; //converts it to a decimal
    
    //asks the user the number of people that went to dinner
    System.out.print ("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt(); //accepts the user's response and stores it as an int
    
    double totalCost; //variable to store the total cost
    double costPerPerson; //variable to store the cost per person
    int dollars, dimes, pennies; //variable used to store whole dollar amount and cents 
    
    totalCost = checkCost * (1 + tipPercent); //calculates the total cost with the tip
    costPerPerson = totalCost / numPeople; //calculates the amount each person pays
    dollars = (int)(costPerPerson);
    dimes= (int)(costPerPerson * 10) % 10; //returns the remainder
    pennies= (int)(costPerPerson * 100) % 10; //returns the remainder to calculate cost per person
    System.out.println ("Each person in the group owes $"+dollars+'.'+dimes + pennies);
  }
}