/////
//CSE 002 Welcome Class
/////
public class WelcomeClass{
  
  public static void main(String args[]){
    System.out.println("  -----------"); //prints the first dotted line
    System.out.println("  | WELCOME |");//prints the welcome line
    System.out.println("  -----------");//prints the second dotted line
    System.out.println("  ^  ^  ^  ^  ^  ^");//prints the carets facing up
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");//prints the slashes
    System.out.println("<-C--H--L--2--2--1->");//Prints my Lehigh network ID 
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");//prints the second line of dashes
    System.out.println("  v  v  v  v  v  v ");//prints the second line of carets
    System.out.println("My name is Damon and I am currently a sophomore majoring in EE");//prints my autobiographic statement
   }
}
