//Damon Luk
//9_10_18
//CSE 002 - Arithmetic

public class Arithmetic {
  
  public static void main (String[]args){
    
    int numPants = 3; //number of pants
    double pantsPrice = 34.98; //Cost per pair of pants
    int numShirts = 2; //number of shirts
    double shirtPrice = 24.99; //Cost per shirt
    int numBelts = 1; //number of belts
    double beltCost = 33.99; //price of one belt
    double paSalesTax = .06; //sales tax in PA
    double totalCostofPants; //total cost of pants
    double totalCostofShirts; //total cost of shirts
    double totalCostofBelt; //total cost of belts
    double totalCost; //the total cost not including sales tax
    double salesTaxofPants; //the sales tax of pants
    double salesTaxofShirts; //the sales tax of shirts
    double salesTaxofBelt; //the sales tax of belt
    double totalSalesTax; //the total sales tax
    double totalCostofPurchase; //everything combined
    
    totalCostofPants = numPants * pantsPrice; //calculates the total cost of pants without tax
    totalCostofShirts = numShirts * shirtPrice; //calculates the total cost of shirts without tax
    totalCostofBelt = numBelts * beltCost; //calculates the total cost of belts without tax
    totalCost = totalCostofPants + totalCostofShirts + totalCostofBelt; //combined total cost without tax
    salesTaxofPants = totalCostofPants * paSalesTax; //calculates the sales tax of pants
    salesTaxofShirts = totalCostofShirts * paSalesTax; //calculates the sales tax of shirts
    salesTaxofBelt = totalCostofBelt * paSalesTax; //calculates the sales tax of belts
    totalSalesTax = salesTaxofPants + salesTaxofShirts + salesTaxofBelt; //total sales tax
    totalCostofPurchase = totalCost + totalSalesTax; //total cost of the purchase, sales tax included
    
    //this part allows my answers to be in two decimal places
    //It multiplies it by a hundred and then casts into an int and then divide by a 100.0
    salesTaxofPants *= 100;
    double salesTaxP = (int) salesTaxofPants;
    salesTaxP /= 100.0;
    salesTaxofShirts *= 100;
    double salesTaxS = (int) salesTaxofShirts;
    salesTaxofShirts /= 100.0;
    salesTaxofBelt *= 100;
    double salesTaxB = (int) salesTaxofBelt;
    salesTaxofBelt /= 100.0;
    totalSalesTax *= 100;
    double totalTax = (int) totalSalesTax;
    totalTax /= 100.0;
    totalCostofPurchase *= 100;
    double totalPurchase = (int) totalCostofPurchase;
    totalPurchase /= 100.0;
    
   
    //prints the statements about total cost and total sales tax 
    System.out.println ("The total cost of the pants were $"+totalCostofPants+" and the sales tax were $"+salesTaxP+" " );
    System.out.println ("The total cost of the shirts were $"+totalCostofShirts+" and the sales tax were $"+salesTaxS+" ");
    System.out.println ("The total cost of the belt was $"+totalCostofBelt+" and the sales tax was $"+salesTaxB+" ");
    System.out.println ("The total cost was $"+totalCost+" and the total sales tax was $"+totalTax+" ");
    
    //prints the total cost of the transaction
    System.out.println ("The total cost of the transaction was $"+totalPurchase+" ");
    
  }
}