//Damon Luk
//10_22_18
//CSE 2 - HW 06
  
  

import java.util.Scanner; //imports the scanner method

public class EncryptedX {
  public static void main (String[]args) {
    Scanner myScanner = new Scanner ( System.in ); //declares the scanner as an instance
    int number = 0; //declares and initiates a variable to store the user's input
    
    
    do { //do while loop to chekc the user's input
      System.out.println ("Please enter an integer between 1 - 100: "); //asks the user to input an integer between 1 - 100
      if (myScanner.hasNextInt() == true ){ //if the input is an integer then it store the input
        number = myScanner.nextInt(); //stores the input
      }
      else {
        System.out.println ("Error! Please input an integer."); //if it is not an input, prints error message
        myScanner.next(); //flushes the user's response and asks for a new one
      }
    }
    while (number > 100 || number < 0); //exists the do while loop when this condition is satisfied
    
    //System.out.println (number);
    
    for ( int numRows = 0; numRows <= number; numRows++ ){ //for loop that starts at zero and runs till it is bigger than the user's input
      for ( int i = 0; i <= number; i++){ //determines the height of the square 
        if ( numRows == i ) { //if numRows = i then it will print a space because that's the main diagonal
          System.out.print (" "); //prints a space
        }
        else if ( i == number - numRows ){ //else if i = number - numRows then it will print a space, other diagonal
          System.out.print (" "); //prints a space
        }
        else {
        System.out.print ("*"); //prints a star if no conditions are met
      }
      }
      System.out.println(); //prints a new line
    }
  }
}
