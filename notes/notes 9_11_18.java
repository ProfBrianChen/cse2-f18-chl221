//operators have precedence and java will automatically calculate according to that precedence
//consider this expression:
double value = 1.0 / 2 + 3 * 4 - 5.0 / 6 + 7 * 8;
//java gives precedence to multiplication and division over addition and subtration
//the above expression is evaluated like this
double value = (1.0 / 2) + (3 * 4) - (5.0 / 6) + (7 * 8);
//For arithmetic oeprators of identical precedence, java evaluates them from left to right 
//this is called "left to right associativity" 
//java seeks to break every expression into a series of "binary operations"
//A + B is a binary operation. A + B + C is not
// "=" is known as the assignment operator
//The assignment operator has the lowest precedence of any operator 
double value = ( ( ((1.0/2)+(3*4)) - (5.0/6)) + (7*8));
//First we do everything on the right side.  Then we evaluate the equals sign and set the variable “value”.
//Unlike the arithmetic operators, the assignment operator is right to left associative
//"int" and "float" are type casting operators
//the type casting operator has the highest precedent we have seen so fat
//type casting is always evaluated FIRST
//For example
//The way you figure out how a complex statement gets evaluated is that you add parentheses until the statement is made up of binary operations.
//Example:
double val1; int val2;
int val3 = (int) val1 * val2 – (int) val1;
//First, the explicit casts.  Highest precedence.
int val3 = ((int) val1) * val2 – ((int) val1);
//Then the multiplication, 2nd precedence
int val3 = ( ((int) val1) * val2 ) – ((int) val1);
//Next, the subtraction, 3rd precedence
int val3 = ( ( ((int) val1) * val2 ) – ((int) val1) );
//Finally, the assignment operator.
( int val3 = ( ( ((int) val1) * val2 ) – ((int) val1) ) ) ;
//casting to an integer always truncates
//Scanner class
//in order to use an existing class you need to 
// 1.) import the class 2.) declare an instance (object) of the class 3.) construct the instance
import java.util.Scanner;	//import
Scanner myScanner; //declare
//construct
myScanner = new Scanner ( System.in );
//classes are pretty much widgets that gives its instances methods 


