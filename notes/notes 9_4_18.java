//Three different numbering systems - base 10, octal and hexa
//Converting numbers to/from different bases
//Input gets transformed into output via an algorithim 
//A variable stores data - each variable stores one input. Can only be declared once
//Three steps: variable declaration, variable assignment, variable access 

float result;           //declared every variable before storing data
float total;            //order matters
total = 125.00;         //float is a type of variable. Variables in java must have a type 
float tip;              //
tip = .15;
result = total * tip;
return result;
 
//declarations must indicate both the type and the identifier
//the command is always: <type><identifier>
//Several types of variables: 
// int - an integer, float - a floating point number, double - a float with extranumerical precision
// boolean - a true or false value, char - an individual letter, string - a list of chars, used to store words
//you generally cannot combine variables of different types
//some number types will convert - type casting
//type casting: implicit and explicit. 
//implicit is automatic while explicit requires you to force them to convert
//implicit happens when it increases in precision eg float automatically casts to doubles and ints to doubles

float var1 = (float) 3.2;
double var2 = 3.3;
int var3 = 1;
boolean var 4 = true;
char var5 = 'c';
System.out.println( var1 + " " + var2 + " " + var3 + " " + var4 + " " + var5 + " ");

//explicit casting
double myDoubleValue = 3.141592; //casting a double to an int
int myInt = (int) myDoubleValue; //casting a double to a float
float myFlt = (float);
myDoubleValue;

//explicit casting will work even on a type that will implicitly cast to another type


