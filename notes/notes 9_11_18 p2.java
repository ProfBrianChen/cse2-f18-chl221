//a literal is a constant value that appears directly in a program
//literals haeva default type
    //integer literals are assumed to be ints
    //floating point literals areassumed to be doubles
//you can specify the type directly
byte age = 43B;
long bytesOfStorage = 1024000000L;
float scale = 44.3F;
double temperature = 88.4D;
//you can also use scientific notation
//the char data type holds single letters
//java can mark some variables as constants which makes them unchangeable
//this is useful if other people are working on your code
//we declare them with final
final double PI = 3.141596;
final byte SHOE_SIZE = 4;
//a method is a container for code
//how to call a method:
<methodName>( <method arguments> );
//methods have high operator precedence 