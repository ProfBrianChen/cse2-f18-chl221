//Damon Luk
//9_15_18
//CSE 002 

import java.util.Scanner; //imports the scanner method

public class Convert{
  
  public static void main (String[]args) {
    Scanner myScanner = new Scanner ( System.in ); //declares an instance of the Scanner
    System.out.print ("Enter the affected area in acres: "); //asks the user to input the area that was affected in acres
    double area = myScanner.nextDouble(); //accepts the user's response and stores it as a double 
    System.out.print ("Enter the rainfall in the affected area: "); //asks the user to input the amount of rain
    double rain = myScanner.nextDouble(); //accepts the user's response and stores it as a double
    
    double gallons; //variable that stores how many gallons of rainfall there are 
    double cubicMiles; //variable that stores how many cubic miles of water there is 
    
    //multiplies area and rain together.
    gallons = area * rain * 27154.29; //multiplies by 27154 which is the amount of rain if it's 1 inch of rain in 1 acre of land
    
    cubicMiles = gallons/ 1.101e12; //divides previous calculation by the amount of gallons in one cubic mile of water
    
    System.out.println (""+cubicMiles+" cubic miles"); //prints the amount of rain in cubic miles
     }
}