//Damon Luk
//9_15_18
//CSE 002 

import java.util.Scanner; //imports the Scanner meethod

public class Pyramid{
  public static void main (String[]args){
    Scanner myScanner = new Scanner ( System.in ); //declares an instance of the scanner
    System.out.print ("The square side of the pyramid is (input length): "); //asks the user to input the length of the sqaure 
    double square = myScanner.nextDouble(); //accepts the user's response and stores it as a double
    System.out.print ("The height of the pyramid is (input height): "); //asks the user to input the height of the pyramid 
    double height = myScanner.nextDouble(); // stores the user's response as a double
    
    double area; //variableble used to store the area of the square
    double volume; //variabel used to store the volume of the pyramid 
    
    area = Math.pow(square, 2); //calculates the area of the square by squaring one side of it 
    volume = (area * height) / 3; //calculates the volume by multiplying area and height then dividing it by 3
    
    System.out.println ("The volume inside the pyramid is: "+volume+""); //prints the final answer 
  }
}