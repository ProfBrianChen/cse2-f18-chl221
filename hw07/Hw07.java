//Damon Luk
//10_28_18
//CSE 2 - HW 07 

import java.util.Scanner; //imports the scanner method 

public class Hw07 {
  public static String sampleText () { //the sample text method that allows the user to enter a text
    Scanner myScanner = new Scanner ( System.in ); //makes the scanner an object 
    String paragraph = ""; //declares the variable paragraph
    
    System.out.println ("Enter a text: "); //prompts the user to enter a text
    paragraph = myScanner.nextLine(); //stores the user's input in the variable declared before
    
    System.out.println ("You entered: " + paragraph); //prints out the user's input
    return paragraph; //returns the user's input back to the main method
 }
  
 public static char printMenu () { //the print menu method that shows the user a menu they can pick from
  Scanner myScanner = new Scanner ( System.in ); //makes the scanner an object
  char input = ' '; //declares the variable input
  
  //prints out the menu and the options the user has 
  System.out.println ("Menu: ");
  System.out.println ("c - Number of non-whitespace characters");
  System.out.println ("w - Number of words");
  System.out.println ("f - Find text");
  System.out.println ("r - Replace all !'s");
  System.out.println ("s - Shorten spaces");
  System.out.println ("q - quit");
  
  System.out.println ("Choose an option: "); //asks the user to pick one of the options 
  input = myScanner.next().charAt(0); //stops the user's input as a char character


 return input; //returns the input to the main method 
 }
 
public static void getNumOfNonWSCharacters ( String paragraph ) { //method that gets the number of characters in the user's input
  int characters = 0; //initiates and declares the variable character 
  int length = paragraph.length(); //defines the variable length as the length of the user's input
  
  for ( int x = 0; x < length; x++) { //for loop that goes from 0 all the way to the length 
    if ( paragraph.charAt(x) != ' ') { //checks to see if that particular position is a space
      characters++; //if its not a space, add to the character count
}
  }  
  System.out.println ("Number of non-whitespace characters: " +characters); //prints out the number of characters
}
  
public static void getNumOfWords (String paragraph) { //method to get the number of words in user's input 
  int words = 1; //initiate and defines the variable words
  int length = paragraph.length(); //defines the variable length as the length of the user's input 
  
  for ( int x = 0; x < length; x++) { //for loop that goes from 0 all the way to the length of the string
    if ( paragraph.charAt(x) == ' ' && paragraph.charAt(x+1) != ' ') { //if the character is a space and the next character is not
      words++; //adds one to the word count
    }
  }
  System.out.println ("Number of words: " + words ); //prints out the number of words 
}
  
public static void findText (String phrase , String paragraph) { //method to find substrings within the user's string
  int index = 0; //sets the variable index at zero
  int occurence = 0; //sets a counter for the number of times a word appears as zero
  
  while ( index != -1 ) { //while loop for while the index is not -1 
    //it will be negative one when the phrase is not found
    index = paragraph.indexOf(phrase, index); //returns the index of the first occurence and continues to go forward to look for the string
    
    if ( index != -1 ) { //if the index is not negative one
      occurence ++; //the counter gets added one
      index += phrase.length(); //the index adds to itself the length of the phrase
    }
  }
  System.out.println ("Number of instances: " + occurence ); //prints out the number of occurence
}
  
public static void replaceExclamation (String paragraph) { //method to replace all exclamation point with periods
  String newParagraph = paragraph.replace('!', '.'); //uses a string method to replace all exclamation with periods
  System.out.println ("Edited text: " + newParagraph); //prints out the new edited text
}
  
public static void shortenSpace ( String paragraph ) { //method to shorten unnecessary spaces in the user's input 
  String edited = paragraph.replaceAll ( "\\s+" , " "); //replaces all double or more spaces with a single space
  //the \s matches a space and the + means one of more
  System.out.println ("Edited text: " + edited); //prints out the edited text
}
  
 public static void main (String[]args) {
    String paragraph = sampleText(); //stores the variable paragraph as the return from the sample text method
    char input = printMenu(); //stores the variable input as the return from the print menu method

    int i = 0; //declares an int i = 0. Used in the while loop
   
    while ( i == 0 ) { //while loop is equal to zero
    if ( input == 'q') { //if the user inputs q
      System.exit (0); //quits the program
      i = 1; //set i = 1 so the loop stops running
    }
    else if ( input == 'c') { //if the user inputs c
      getNumOfNonWSCharacters ( paragraph ); //uses the get number of character method with the input being the user's input 
      input = printMenu(); //prints the menu again for the user
    }
    else if ( input == 'w') { //if the user inputs w
      getNumOfWords ( paragraph ); //uses the get number of words method with the user's input
      input = printMenu(); //prints the menu again for the user       
    }
    else if ( input == 'f') { //if the user inputs f
      Scanner myScanner = new Scanner ( System.in ); //declares the scanner as an object
      System.out.println ("Enter a word or phrase to be found: "); //asks the user to input a phrase 
      String phrase = myScanner.nextLine(); //stores that phrase
      findText (phrase, paragraph); //uses the find text method to find the phrase in the user's input
      input = printMenu(); //prints the menu again for the user
    }
    else if ( input == 'r') { //if the user inputs r
      replaceExclamation ( paragraph ); //uses the replace exclamation method with input variable paragraph
      input = printMenu(); //prints the menu again for the user
    }
    else if ( input == 's') { //if the user inputs s
      shortenSpace ( paragraph ); //uses the shorten space method with input variable paragraph
      input = printMenu(); //prints the menu again for the user
    } 
   else {
     System.out.println ("Error. Please enter appropriate input."); //tells the user their input is invalid
     input = printMenu(); //prints the menu again for the user
   }
  }
}
}