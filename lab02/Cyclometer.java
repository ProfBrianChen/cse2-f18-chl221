//Damon Luk 
//9_6_18
//CSE 002 - Cyclometer
public class Cyclometer {
  
  public static void main (String[]args) { 
    int secsTrip1 = 480; //how many seconds trip 1 took
    int secsTrip2 = 3220; //how many seconds trip 2 took
    int countsTrip1 = 1561; //number of tire rotations in trip 1
    int countsTrip2 = 9037; //number of tire rotations in trip 2
    double wheelDiameter = 27.0; //the diameter of the tire
    double PI = 3.14159; //this stores the value of pi
    int feetPerMile = 5280; //how many feet is in one mile
    int inchesPerFoot = 12; //how many inches is in one foot
    int secondsPerMinute = 60; //how many seconds in one minute
    double distanceTrip1, distanceTrip2, totalDistance; //creating variables to store calculated numbers
    System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts."); 
	  System.out.println("Trip 2 took "+ (secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts.");
    //how long each trip took and how many counts each trip had 
    
    distanceTrip1 = countsTrip1 * wheelDiameter * PI; //this calculates the distance of trip1 in inches 
    distanceTrip1 /= inchesPerFoot * feetPerMile; //this converts the above calculation into miles
    distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile; //this calculates the distance of trip2 in inches
    totalDistance = distanceTrip1 + distanceTrip2; //this converts the above calculation into miles
    
    System.out.println("Trip 1 was "+distanceTrip1+" miles"); //this prints the distance of trip1 
    System.out.println("Trip 2 was "+distanceTrip2+" miles"); //this prints the distance of trip2
    System.out.println("The total distance was "+totalDistance+" miles"); //this prints the total distance
  }
}
