//Damon Luk
//11_27_18
//CSE 2 - HW 09 Program 2 

import java.util.Scanner; //imports the scanner method
import java.util.Random; //imports the random method


public class removeElements{
  public static void main(String [] args){
	
  Scanner scan = new Scanner(System.in); //makes an object of the scanner
  
  int num[] = new int[10]; //allocates an array with size 10
  int newArray1[]; //declares an array
  int newArray2[]; //declares an array
  int index; //declares a variable index
  int target; //declares a variable target
	String answer = ""; //declares a variable string
	
  do{ //do while loop
  	
    System.out.print("Random input 10 ints [0-9]"); //lets the user know that this is the array
  	num = randomInput(); //uses randomInput method to generate a random array with 10 elements
  	String out = "The original array is:"; //lets the uer know the original array
  	out += listArray(num); //uses listArray method to print out the generated array
  	System.out.println(out); //prints the array
 
  	System.out.print("Enter the index "); //prompts the user to enter the index
  	index = scan.nextInt(); //accepts the user's request
  	newArray1 = delete(num,index); //uses delete method to make a new array without the value at the provided index
  	String out1="The output array is "; //lets the user know the new array
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1); //prints the new array
 
    System.out.print("Enter the target value "); //prompts the user for a target value
  	target = scan.nextInt(); //accepts the user's request
  	newArray2 = remove(num,target); //uses remove method to remove the number the user provided
  	String out2="The output array is "; //lets the user know the new array
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2); //prints the new array
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-"); //asks the user if they want to do it again
  	answer = scan.next(); //accepts the user's answer
	
  }while(answer.equals("Y") || answer.equals("y")); //the loop continues until this condition is false
  }
 
  public static String listArray (int num[]) { //listArray method
	String out = "{"; //declares variable out
	for(int j = 0; j < num.length; j++){ //for loop that goes from 0 to the length of the array
  	if( j > 0 ){ //if j is greater than zero
    	out += ", "; //varible out gets added to
  	}
  	out += num[ j ]; //variable out gets added to
	}
	out += "} "; //variable out gets added to
	return out; //returns out to the main method
  }
   
public static int [] randomInput () { //randomInput method
  int randomArray [] = new int [10]; //allocates a new array with size 10
  Random rand = new Random (); //makes an object of the random method
  for ( int i = 0; i < randomArray.length; i ++) { //for loop goes from 0 to size of the array
    int number = rand.nextInt (10); //generates a random number from 0 to 9
    randomArray[i] = number; //sets the index i of the array to random number
  }
  return randomArray; //returns the random array to main method
}
  
public static int [] delete (int original [], int pos) { //delete method
  
  int [] shortArray = new int [original.length - 1]; //allocates a new array with size one less than the original
  
  boolean old = true; //sets a boolean variable to true
 
  if ( pos > original.length || pos < 0) { //if the index provided is greater than the size or less than zero
    System.out.println ("The index is not valid."); //Error message because the index is not valid
    old = true; //old is still true
  }
  else { //if the index is within range
  for ( int x = 0; x < pos; x++) { //goes from 0 to the index
    shortArray[x] = original[x]; //sets every value the same
  } 
  for ( int z = pos ; z < shortArray.length; z++) { //goes from the index to the size of the shortened array
    shortArray[z] = original[ z + 1 ]; //the value in the short array is one behind the original array
  }
    old = false; //sets old to false
  }
 if ( old == false ){ //if old is false then the element was removed
 System.out.println ("Index " + pos + " element is removed."); //prints that the element was removed
 return shortArray;  //returns the short array
}
  else{ //if the above condition is not met the index was not found
    return original; //returns original
  } 
}
  
public static int [] remove (int[] list, int target){ //remove method
  int times = 0; //declares a variable to keep track of the number of times the target appears
  boolean found = false; //sets a boolean variable to false
  for ( int w = 0; w < list.length; w++ ) { //for loop that goes from 0 to the size of the array
    if (list[w] == target) { //if any values in the array is equal to the value
      times++; //times is incremented
    }
  }
  System.out.println (times); //prints times to make sure it works
  
  int remove [] = new int [ list.length - times ]; //sets the new array to the right size
    
    for ( int k = 0; k < list.length; k++){ //goes from 0 to the size of the original array
    
      if (list[k] == target) { //if it finds the target number as it iterates through each of the elements in the array
      found = true; //sets found to true
      for (int t = 0; t < k; t++ ) { //for loop that goes from 0 to index of target
      remove [t] = list [t]; //sets every value of remove to same as list up till the target
      }      
      for ( int r = k; r < remove.length; r ++) { //for loop that goes from index to the length of the new array
      remove [ r ] = list [ r + 1 ]; //sets every value of new array to previous value in old array
      }
      
    }    
  }
  
    if ( found == true) { //if found is true then the number was found
      return remove; //return remove to the main program
    }
  else { //if found is false then the number was not found
    System.out.println ("Element " + target + " was not found."); //tells the user the number was not found
    return list; //returns the original array
  }
}
}
  