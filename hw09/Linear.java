//Damon Luk
//11_26_18
//CSE 2 - HW 09 Program 1

import java.util.Scanner; //imports the scanner method
import java.util.Random; //imports the random method

public class Linear {
  public static void main (String [] args) {
    Scanner myScanner = new Scanner (System.in); //makes an object of the scanner
    int [] grades = new int [15]; //creates a new array with 15 size
    System.out.println ("Enter 15 ascending ints for final grades in CSE 2: "); //prompts the user to enter 15 grades
    for (int i = 0; i < grades.length; i++) { //for loop that goes through every member of the array
      if ( myScanner.hasNextInt() == true) { //if it is an integer then this if statement executes
        int input = myScanner.nextInt(); //saves the number as a variable
        if (input > 0 && input < 100) { //checks to see if the number is between 0 and 100
          if ( i == 0 || grades [ i - 1 ] <= input ) { //checks to see if the number is in ascending order
            grades [i] = input; //if all the conditions are met, it initializes every member of the array with the input
          }
          else { //if it is not in ascending order
            System.out.println ("Error. Your input needs to be in ascending order"); //error message
            myScanner.next(); //flushes the user's input out
            i--; //decrements i by one so it gets 15 entries at the end 
          }
        }
        else { //if the number is not within range
          System.out.println ("Error. Your input needs to be between 0 and 100."); //error message
          myScanner.next(); //flushes the user's input out
          i--; //decrements i by one so it gets 15 entries at the end
        }
      }
      else { //if the input is not an integer
        System.out.println ("Error. Your input must be an integer."); //error message
        myScanner.next(); //flushes the user's input out
        i--; //decrements i by one so it gets 15 entries at the end 
      }
    }
    for (int j = 0; j < grades.length; j++) { //a for loop that goes from 0 to the size of the array
      System.out.print (grades[j] + " "); //prints out every element in the array
   }
    System.out.println(); //prints a new line
    
    System.out.println ("Enter a grade to search for: "); //prompts the user for a grade to search for
    int target = myScanner.nextInt(); //stores the user's input as the target number
    binarySearch (target, grades); //uses binary search method to find the number in the array
    
    int mixedArray[] = scramble (grades); //use the scramble method to shuffle the contents of the array
    System.out.println ("Scrambled: "); //prints out the words scrmable
    for ( int x = 0; x < mixedArray.length; x++ ) { //for loop that goes from 0 to size of the array
      System.out.print (mixedArray[x] + " "); //prints out every element in the array
    }
    System.out.println (); //prints a new line
    
    System.out.println ("Enter a grade to search for: "); //prompts the user for a grade to search for
    int search = myScanner.nextInt(); //stores the user's input as the search number
    linearSearch (search, mixedArray); //uses the linear search method to find the number in the mixed array
  }
    
  public static void binarySearch (int number, int [] list) { //binary search method
    int iteration = 0; //declares and initializes a varaible to keep count of the iterations
    boolean found = false; //declares a boolean variable found to be false
    int start = 0; //declares abd initializes the start to be 0
    int end = list.length - 1; //declares and initializes the end to be the size of the array minus 1
    int middle = (int) (start + end) / 2; //declares and initializes the middle to be halfway between start and end
    while ( middle <= end || middle >= start) { //while loop that runs until middle is bigger than end or samller than start
      if ( number == list[middle]) { //if the number is the value in the middle
        found = true; //sets boolean value to true
        break; //breaks out of the loop
      }
     if ( number > list[middle]) { //if the number is greater than the value in the middle
        middle = middle + 1; //move middle up by one
        iteration++; //add one iteration
      }
      if ( number < list[middle]) { //if the number is less than the value in the middle
        middle = middle - 1; //move middle down by one
        iteration++; //add one iteration
      }
    }
    if (found = true) { //if after the while loop the boolean value is true then the number was found
      System.out.println (number + " was found with " + iteration + " iterations."); //prints the number of iterations it took
    }
    else { //if the boolean value remains false then the number was not found
      System.out.println (number + " was not found with " + iteration + " iterations."); //prints the number of iterations it took
    } 
  }
   
  public static int [] scramble ( int array [] ) { //method to scramble the contents of the original array
    Random randIndex = new Random (); //creatse an object of the random method
    for ( int i = 0; i < 100; i ++) { //for loop that goes from 0 to 100
      int random = randIndex.nextInt (array.length); //random number from 0 to the length of the array
      int swap = array [0]; //creates a temporary variable to store the value at index 0
      array [0] = array [random]; //sets the value of index 0 to a random index in the array
      array [random] = swap; //sets the value of the random index to what was originally in index 0
    }
    return array; //returns the array to main method   
  }
    
 public static void linearSearch (int goal, int mixed [] ) { //linear search method
   int iteration2 = 0; //declares and initializes a variable to keep track of iterations
   boolean foundIt = true; //sets a boolean variable foundIt to be true
    for (int n = 0; n < mixed.length; n++) { //goes through from 0 to the size of the mixed array
      if ( goal == mixed[n] ) { //if the number is equal to the value at array [n]
        iteration2 = n; //sets iteration to n 
        foundIt = true; //sets found it to true
        break; //breaks out of the loop
      }
      else { //if the above condition is not satisfied 
        foundIt = false; //sets found it to false
        iteration2 = n; //sets iteration to n
      }
    }
   if (foundIt == true) { //at the end if the boolean value is true the number was found
     System.out.println (goal + " was found with " + iteration2 + " iterations."); //prints the number of iterations
   }
   else { //if the boolean value is false then the number was not found
     System.out.println (goal + " was not found with " + iteration2 + " iterations."); //prints the number of iterations
   }
   }
}

 